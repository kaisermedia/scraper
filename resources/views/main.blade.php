<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Main page</title>

    <!-- Bootstrap -->
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">!-->
    {{ Html::style('css/bootstrap.min.css') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="container">

      <div class="col-md-8">
        <h1>Scraper prototype </h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quam obcaecati amet aliquam officiis est ducimus voluptate, sunt quae, nihil veritatis earum. Quaerat ut delectus voluptatum repudiandae quia, laboriosam perspiciatis.</p>
        <hr />

        @if(Session::has('success'))
          <div class="alert alert-success">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <strong>{{ Session::get('success') }}</strong>
          </div>
        @endif

        <div class="row">
          <h2 class="pull-left">Current jobs</h2>
          <a href="/fire" class="pull-right btn btn-success" style="margin-left: 12px;">Fire!</a>
          <a href="/reset" class="pull-right btn btn-danger" style="margin-left: 12px;">Reset</a>
          {{--<button type="button" class="pull-right btn btn-primary">Add Job</button>--}}
        </div>
        <table class="table">
        <tr>
          <td>Target URL</td>
          <td>Status</td>
          <td>Data</td>
        </tr>

          @foreach ($urls as $url)
            <tr>
                <td>{{ $url['url'] }}</td>
                <td>{{ $url['status'] }}</td>
                <td>
                    @if( $url['status_code'] == 1)
                        <span class="glyphicon glyphicon-hourglass"></span>
                    @else
                        <a href="{{ url('/url/'.$url['id']) }}" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list-alt"></span></a>
                    @endif
                </td>
            </tr>
          @endforeach
        </table>

      </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    {{ Html::script('js/bootstrap.min.js') }}
  </body>
</html>