<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Main page</title>

    <!-- Bootstrap -->
    <!--<link href="css/bootstrap.min.css" rel="stylesheet">!-->
    {{ Html::style('css/bootstrap.min.css') }}

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">

    <div class="col-md-8">
        <h1>Scraper prototype </h1>

        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut quam obcaecati amet aliquam officiis est ducimus voluptate, sunt quae, nihil veritatis earum. Quaerat ut delectus voluptatum repudiandae quia, laboriosam perspiciatis.</p>
        <hr />
        <div class="row">
            <h2 class="pull-left">Current jobs</h2>
            <button type="button" class="pull-right btn btn-primary">Add Job</button>
        </div>
        <h1>Vacature(s) {{ $size }}</h1>

        <table class="table">
            <tr>
                <td>Data</td>
                <td>Bron</td>
            </tr>

            @foreach ($data as $vac)
                <tr>
                    <td>{{ $vac['data'] }}</td>
                    <td>{{ $vac['reference_url'] }}</td>
                </tr>
            @endforeach
        </table>

    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
{{ Html::script('js/bootstrap.min.js') }}
</body>
</html>