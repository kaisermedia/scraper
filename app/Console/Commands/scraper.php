<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class scraper extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:run {--dryrun}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pulls the data that was entered into the front end and runs them through a scraper';

    /**
     * Ref to Goute
     */
    protected $client;

    /**
     * Ref to Class
     */
    protected $parser;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        /**
         * Just for clarity, passing $this into child classes so i dont have to extend the console
         * and the components only need the CLI data anyway ;-)
         */

        // init the parser
        $this->parser = new \App\Scraper\parser($this);

        // init the spider
        $this->spider = new \App\Scraper\spider($this);

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Check all urls that have status set to 1
         * (hint: status is a text field for user view )
         */
        $items = \App\urlstore::get()->where('status_code',1)->toArray();

        /**
         * Parse the verbose flag and notice user
         * Just useful for debugging / testing
         */
        $verbose = $this->option('verbose') ? true : false;
        if($verbose) $this->warn('Running in verbose mode');

        /**
         * This is where the magic is launching.
         * loop over the table data and check
         * if we should spider and then scrape or only scrape
         */

        if(count($items)<=0) {
            $this->error('Nothing to do... ');
            return true;
        }

        #for($i=0;$i<=count($data)-1;$i++) {

        foreach($items as $data) {

            $this->info('Parsing URL '.$data['url']);

            /**
             * Grep ID so we can write back to database with corresponding ID
             */
            $id = $data['id'];

            /**
             * Spiderman! Spiderman! Does whatever a spider can!
             */
            if($data['spider'])
            {
                /**
                 * Crawl the page for all possible URL's
                 */
                $web = $data['url'];
                $list = $this->spider->_crawl($web); // Coudn't resist a semi phun

                /**
                 * Iterate over the URL's and toss them into the scraper
                 */
                foreach($list as $key => $item)
                {
                    /**
                     * Just some crap filtering
                     */
                    if($key == "javascript://") { continue; }
                    if(stristr($key,"contact")) { continue; }

                    /**
                     * Dont go check external websites...
                     */
                    if($item['external_link']) {
                        if($verbose) $this->info("- Skipping external link");
                        continue;
                    }

                    if($verbose) $this->info('Parsing '.$item['absolute_url']);

                    /**
                     * Toss the absolute_url into the scraper
                     */
                    $data = $this->parser->_scrape($item['absolute_url'],$verbose);

                    /**
                     * The scrape function returns false if the page
                     * is to short to scrape or invalid
                     */
                    if($data === false ) { continue; }

                    /**
                     * use the laravel helper to get the first entry (highest score)
                     */
                    $result = head($data);

                    /**
                     * In case the head function fails
                     */
                    if($result === false)
                    {
                        continue;
                    }

                    #var_dump($result);

                    /**
                     * Store data
                     */

                    $db = new \App\vacancys();

                    $db->urlstore_id    =   $id;
                    $db->reference_url  =   $item['absolute_url'];
                    $db->data           =   $result["data"];

                    $db->save();

                }

                /**
                 * Update the status
                 */
                $x = new \App\urlstore();
                $x->where('id',$id)->update(['status_code' => 2,'status' => 'complete']);

                /**
                 * End of IF
                 */

            } else {
                /**
                 * In case we do not need to spider but just get a URL
                 */
                $content = $this->parser->_scrape($data['url'],$verbose);
                $result = head($content);

                /**
                 * Store data
                 */
                if(strlen($result['data'])>120) {
                    $db = new \App\vacancys();

                    $db->urlstore_id = $data['id'];
                    $db->reference_url = $data['url'];
                    $db->data = $result["data"];

                    $db->save();

                    /**
                     * Update the status
                     */
                    $x = new \App\urlstore();
                    $x->where('id', $data['id'])->update(['status_code' => 2, 'status' => 'complete']);
                }

                #$db->id = $data['id'];
                #$db->

                /**
                 * Done
                 */
            }


        }
    }
}
