<?php

namespace App\Scraper;

class spider
{
    var $client;

    public function __construct($cli)
    {
        // Init the shared class
        $shared = new shared();

        // Get client
        $this->client = $shared->client;

        // Dependency injection FTW!
        $this->cli = $cli;

        $this->base_url = "";

    }

    /**
     * Crawl the page and return most of the links in the page
     *
     * @param $url
     * @return array
     */
    public function _crawl($url)
    {
        // Set base URL on class
        $this->base_url = $url;

        // Get the page
        $data = $this->client->request('get',$url);

        // Get all Links
        // Thanks fly out ot
        // http://zrashwani.com/simple-web-spider-php-goutte/#.V1aJZpOLRTY
        $links = $this->extractLinksInfo($data,$url);

        // ...
        return $links;
    }

    /**
     * checks the uri if can be crawled or not
     * in order to prevent links like "javascript:void(0)" or "#something" from being crawled again
     * @param string $uri
     * @return boolean
     */
    protected function checkIfCrawlable($uri) {
        if (empty($uri)) {
            return false;
        }

        $stop_links = array(//returned deadlinks
            '@^javascript\:void\(0\)$@',
            '@^#.*@',
        );

        foreach ($stop_links as $ptrn) {
            if (preg_match($ptrn, $uri)) {
                return false;
            }
        }

        return true;
    }

    /**
     * check if the link leads to external site or not
     * @param string $url
     * @return boolean
     */
    public function checkIfExternal($url) {
        $base_url_trimmed = str_replace(array('http://', 'https://'), '', $this->base_url);

        if (preg_match("@http(s)?\://$base_url_trimmed@", $url)) { //base url is not the first portion of the url
            return false;
        } else {
            return true;
        }
    }

    protected function normalizeLink($uri) {
        $uri = preg_replace('@#.*$@', '', $uri);
        return $uri;
    }

    /**
     * extracting all <a> tags in the crawled document,
     * and return an array containing information about links like: uri, absolute_url, frequency in document
     * @param Symfony\Component\DomCrawler\Crawler $crawler
     * @param string $url_to_traverse
     * @return array
     */
    protected function extractLinksInfo(\Symfony\Component\DomCrawler\Crawler &$crawler, $url_to_traverse) {
        $current_links = array();
        $crawler->filter('a')->each(function(\Symfony\Component\DomCrawler\Crawler $node, $i) use (&$current_links) {
            $node_text = trim($node->text());
            $node_url = $node->attr('href');
            $hash = $this->normalizeLink($node_url);

            if (!isset($this->site_links[$hash])) {
                $current_links[$hash]['original_urls'][$node_url] = $node_url;
                $current_links[$hash]['links_text'][$node_text] = $node_text;

                if (!$this->checkIfCrawlable($node_url)){

                }elseif (!preg_match("@^http(s)?@", $node_url)) { //not absolute link
                    $current_links[$hash]['absolute_url'] = $this->base_url . $node_url;
                } else {
                    $current_links[$hash]['absolute_url'] = $node_url;
                }

                if (!$this->checkIfCrawlable($node_url)) {
                    $current_links[$hash]['dont_visit'] = true;
                    $current_links[$hash]['external_link'] = false;
                } elseif ($this->checkIfExternal($current_links[$hash]['absolute_url'])) { // mark external url as marked
                    $current_links[$hash]['external_link'] = true;
                } else {
                    $current_links[$hash]['external_link'] = false;
                }
                $current_links[$hash]['visited'] = false;

                $current_links[$hash]['frequency'] = isset($current_links[$hash]['frequency']) ? $current_links[$hash]['frequency']++ : 1; // increase the counter
            }

        });

        if (isset($current_links[$url_to_traverse])) { // if page is linked to itself, ex. homepage
            $current_links[$url_to_traverse]['visited'] = true; // avoid cyclic loop
        }
        return $current_links;
    }
}