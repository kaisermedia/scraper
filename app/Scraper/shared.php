<?php

namespace App\Scraper;

/**
 * Class shared
 *
 * Just for some function that the scraper and the spider have in common
 * and some functions that are just usefull to have :-)
 *
 * @package App\Scraper
 */
class shared
{
    /**
     * Public ref to Goutte
     * See: https://github.com/FriendsOfPHP/Goutte
     *
     * @var Client
     */
    var $client;

    public function __construct()
    {
        // Init the Goute client.
        $this->client = new \Goutte\Client();
    }

    /**
     * Cleans up a string by removing whitespace and HTML tags
     *
     * @param $data
     * @return string
     */
    public function cleanString($data) {
        return strtolower(preg_replace('/\s+/', ' ',strip_tags($data)));
    }
}