<?php

namespace App\Scraper;

class parser
{

    /**
     * Keywords to check on
     */
    protected $keywords = ['ons team','per week','aanbod','contract','overeenkomst','arbeidsvoorwaarden','doorgroeimogelijkheden','CV','curriculum','mbo','werktijden','Functie-eisen','acquisitie'];

    /**
     * parser constructor.
     *
     * Gets the $this propperty from its parent so i dont
     * have to extend laravel components to get this running
     *
     * @param $cli
     */
    public function __construct($cli)
    {
        // Init the shared class
        $this->shared = new shared();

        // Extract client from shared
        $this->client = $this->shared->client;

        // Dependency injection FTW!
        $this->cli = $cli;
    }

    /**
     * Gets called by the _findContainer function with
     * the $el variable containing the piece
     * of HTML to check against the keywords
     *
     * @param $el
     * @param $verbose
     * @return float|int
     */
    public function _stringCompare($el,$verbose)
    {
        # Humans dont read HTML
        # Nor excessive spaces and lowercase just for easyser comparison
        $data = $this->shared->cleanString($el->nodeValue);

        # Quick and dirty wordcounter
        $wordcount = count(explode(' ', $data));

        # Minimal amount of words for something to be of interest
        if($wordcount < 150) {
            if($verbose) {
                $this->cli->warn('Block is to short. skipping');
            }
            return 0;
        }

        # Wordcount can be relevant for debugging
        if ($verbose) $this->cli->line('Container has ' . $wordcount . ' words');

        # Set score to 0 and add if keyword for matches
        $score = 0;

        # Check the actual keywords
        foreach ($this->keywords as $keyword) {

            # Just to be sure
            $keyword = strtolower($keyword);

            # Check if keyword is in current object
            if (stripos($data,$keyword) !== false) {
                $score++;
            }
        }

        # If half of the keywords or more are in
        # Artificially bump up the score
        if(count($this->keywords)/2 >= $score) {
            $score = $score++;
        }

        # Just a simple percentage calculation
        $percentage = (100/$wordcount*$score);

        # Output score data for debugging purpose
        if($verbose) {
            $this->cli->line("Block score " . $score . " ");
            $this->cli->line("Final score: " . round($percentage, 2) . "%");
        }

        return $percentage;
    }

    /**
     * Track down the container that most probbaly
     * contains the data we're looking for
     *
     * @param $data
     * @param bool $verbose
     * @return array
     */
    public function _findContainer($data,$verbose=false)
    {
        # Extract all div's
        # (eventualy everthing goes into a div, just to chop the page up into smaller pieces)
        $nodeList = $data->filter('div');

        # Place to store data to
        $entry = [];

        foreach($nodeList as $el) {
            $calc = $this->_stringCompare($el,$verbose);

            if($calc>0) {
                $entry[] = [
                    'score' =>  $calc*100, //usort doenst check after the decimal seperator for some reason
                    'data'  =>  $this->shared->cleanString($el->nodeValue)
                ];
            }
        }

        /**
         * Sort array on score
         */
        usort($entry, function($a, $b) {
            return $b['score'] - $a['score'];
        });

        $entry = array_unique($entry,SORT_REGULAR); // SEO extension tend to "duplicate" content for some reason (??)

        return $entry;
    }

    /**
     * Scrape the page
     * Filters out the content that might be of interest of us.
     *
     * @param $url
     * @param $verbose
     * @return array
     */
    public function _scrape($url,$verbose)
    {
        // Get entire page
        $crawler = $this->client->request('GET',$url);

        // Get the most likely container from the DOM
        $container = $this->_findContainer($crawler,$verbose);

        return $container;
    }
}