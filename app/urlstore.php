<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class urlstore extends Model
{
    //
    protected $table = 'urlstore';

    protected $fillable = ['status_code']; // For DEMO stuff

    public function vacancys()
    {
        return $this->hasMany('\App\vacancys');
    }
}
