<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::macro('after', function ($callback) {
    $this->events->listen('router.filter:after:newrelic-patch', $callback);
});

Route::get('/', function () {
    $data = \App\urlstore::all();
    return view('main',['urls'=>$data]);
});


Route::get('/url/{id}','urlController@view');

/**
 * For demo purpose !
 */
Route::get('/fire','fireController@index');
Route::get('/reset','fireController@reset');

