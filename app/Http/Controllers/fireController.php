<?php

namespace App\Http\Controllers;

use App\urlstore;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;

class fireController extends Controller
{
    public function index()
    {
        Artisan::call('scraper:run');

        return redirect()->back()->with([
            'success' => 'Vacatures zijn gescraped'
        ]);
    }

    public function reset() {

        // Just a quick and dirty mod for demo-reset

        // reset status codes
        \App\urlstore::find(1)->update(['status_code'=>'1', 'status' => 'incomplete']);
        \App\urlstore::find(2)->update(['status_code'=>'1', 'status' => 'incomplete']);

        // Truncate the vacanys table
        \App\vacancys::truncate();

        return redirect()->back()->with([
            'success' => 'Vacatures zijn verwijderd'
        ]);
    }
}
